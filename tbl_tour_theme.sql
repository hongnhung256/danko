/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50621
 Source Host           : localhost:3306
 Source Schema         : danko

 Target Server Type    : MySQL
 Target Server Version : 50621
 File Encoding         : 65001

 Date: 30/08/2018 05:55:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_tour_theme
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tour_theme`;
CREATE TABLE `tbl_tour_theme`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `banner` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `lang` enum('vi','en','fr') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tbl_tour_theme_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_tour_theme
-- ----------------------------
INSERT INTO `tbl_tour_theme` VALUES (8, 'Tour trăng mật', 'tour-trang-mat', '/upload/2017/12/1513837886-anh6jpg', '/upload/2017/12/1513840011-bannerjpg', 'Tour trăng mật', 'Tour trăng mật', 'Tour trăng mật', 'vi', '2017-03-07 13:45:09', '2018-08-30 01:12:08');
INSERT INTO `tbl_tour_theme` VALUES (9, 'Tour lễ hội', 'tour-le-hoi', '/upload/2017/03/1489371492-honey_moon.jpg', '/upload/2017/03/1489371492-honeyyymoon.jpg', 'Tour lễ hội', 'Tour lễ hội', 'Tour lễ hội', 'vi', '2017-03-07 13:45:35', '2018-08-30 01:12:37');
INSERT INTO `tbl_tour_theme` VALUES (11, 'Tour biển đảo', 'tour-bien-dao', '/upload/2017/12/1513838675-anh4jpg', '/upload/2017/03/1489245723-du lịch st.jpg', 'Tour biển đảo', 'Tour biển đảo', 'Tour biển đảo', 'vi', '2017-03-11 00:22:03', '2018-08-30 01:12:53');
INSERT INTO `tbl_tour_theme` VALUES (12, 'Tour sinh thái', 'tour-sinh-thai', '/upload/2017/12/1513838692-anh5jpg', '/upload/2017/12/1513838692-banner3jpg', 'Tour sinh thái', 'Tour sinh thái', 'Tour sinh thái', 'vi', '2017-03-12 12:26:32', '2018-08-30 01:13:08');
INSERT INTO `tbl_tour_theme` VALUES (13, 'Tour tâm linh', 'tour-tam-linh', '/upload/2017/12/1513838715-anh5jpg', '/upload/2017/12/1513838715-bannerjpg', 'Tour tâm linh', 'Tour tâm linh', 'Tour tâm linh', 'vi', '2017-03-12 12:33:44', '2018-08-30 01:13:21');
INSERT INTO `tbl_tour_theme` VALUES (14, 'Tour gia đình', 'tour-gia-dinh', '/upload/2017/12/1513838763-tour1jpg', '/upload/2017/03/1489375356-hội an.jpeg', 'Tour gia đình', 'Tour gia đình', 'Tour gia đình', 'vi', '2017-03-12 13:22:37', '2018-08-30 01:14:04');
INSERT INTO `tbl_tour_theme` VALUES (16, 'Classic Journeys', 'classic-journeys', '/upload/2017/12/1513838795-tour2jpg', '/upload/2017/04/1491279280-hot-spotjpg', 'Classic Journeys', 'Classic Journeys', 'Classic Journeys', 'en', '2017-04-03 21:06:44', '2017-12-20 22:46:35');
INSERT INTO `tbl_tour_theme` VALUES (17, 'Homestay', 'homestay', '/upload/2017/04/1491279305-black-hmong-group1-compressedjpg', '/upload/2017/04/1491279306-mien-bacjpg', 'Homestay', 'Homestay', 'Homestay', 'en', '2017-04-03 21:08:02', '2017-12-17 23:23:12');
INSERT INTO `tbl_tour_theme` VALUES (18, 'Short Trips', 'short-trips', '/upload/2017/04/1491880227-phong-nha-ke-bang-1jpg', '/upload/2017/04/1491880227-adventurejpg', 'Short Trips', 'Short Trips', 'Short Trips', 'en', '2017-04-10 20:10:28', '2017-12-17 23:23:26');

SET FOREIGN_KEY_CHECKS = 1;
