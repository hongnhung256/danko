/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50621
 Source Host           : localhost:3306
 Source Schema         : danko

 Target Server Type    : MySQL
 Target Server Version : 50621
 File Encoding         : 65001

 Date: 30/08/2018 07:01:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_setting
-- ----------------------------
DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE `tbl_setting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tbl_setting_id_uindex`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_setting
-- ----------------------------
INSERT INTO `tbl_setting` VALUES (1, 'featured_tour_taxonomy', 'null');
INSERT INTO `tbl_setting` VALUES (2, 'featured_tour', 'null');
INSERT INTO `tbl_setting` VALUES (3, 'featured_theme', 'null');
INSERT INTO `tbl_setting` VALUES (4, 'featured_news', 'null');
INSERT INTO `tbl_setting` VALUES (6, 'base_url', 'http://aatour');
INSERT INTO `tbl_setting` VALUES (7, 'home_banner', '/upload/2017/05/1495007458-at-hoa-tam-giac-mach-ha-giang-e434277c0485358b240a0f3682f90e38jpg');
INSERT INTO `tbl_setting` VALUES (8, 'about_banner', '/upload/2017/02/1487954066-bg-intro.jpg');
INSERT INTO `tbl_setting` VALUES (9, 'news_banner', '/upload/2017/03/1489460772-tour-tet_06.jpg');
INSERT INTO `tbl_setting` VALUES (10, 'creat_tour_banner', '/upload/2017/05/1494908279-1387252932tamgiacmach5jpg');
INSERT INTO `tbl_setting` VALUES (11, 'setting_email', NULL);
INSERT INTO `tbl_setting` VALUES (12, 'setting_tel', NULL);
INSERT INTO `tbl_setting` VALUES (13, 'home_meta_title', 'Danko travel');
INSERT INTO `tbl_setting` VALUES (14, 'home_meta_keyword', '3 ngày 2 đêm,4 ngày 3 đêm');
INSERT INTO `tbl_setting` VALUES (15, 'home_meta_description', 'Danko travel');
INSERT INTO `tbl_setting` VALUES (16, 'featured_tour_taxonomy_en', 'null');
INSERT INTO `tbl_setting` VALUES (17, 'featured_tour_en', '[\"165\",\"164\",\"159\"]');
INSERT INTO `tbl_setting` VALUES (20, 'featured_news_en', '[\"7\",\"51\",\"12\",\"13\"]');
INSERT INTO `tbl_setting` VALUES (21, 'featured_theme_en', '[\"18\",\"17\",\"16\"]');
INSERT INTO `tbl_setting` VALUES (22, 'home_meta_title_en', 'Danko travel');
INSERT INTO `tbl_setting` VALUES (23, 'home_meta_keyword_en', '');
INSERT INTO `tbl_setting` VALUES (24, 'home_meta_description_en', '');
INSERT INTO `tbl_setting` VALUES (25, 'slider_text', '[null,null,\"098985 359\",null,null,null,null,\"https:\\/\\/www.facebook.com\\/dankotravelvietnam\",\"<div class=\\\"footer-contact-container\\\">c&ocirc;ng ty cp th\\u01b0\\u01a1ng m\\u1ea1i v&agrave; d\\u1ecbch v\\u1ee5 du l\\u1ecbch anh anh \\u0111\\u1ecba ch\\u1ec9: 207 - 209 tr\\u1ea7n \\u0111\\u0103ng ninh, d\\u1ecbch v\\u1ecdng, c\\u1ea7u gi\\u1ea5y, h&agrave; n\\u1ed9i \\u0110i\\u1ec7n tho\\u1ea1i: <a href=\\\"tel:0243 793 9363\\\">0243 793 9363<\\/a> Email: <a href=\\\"#\\\">trangdo@aatour.vn<\\/a><\\/div>\\r\\n\"]');
INSERT INTO `tbl_setting` VALUES (26, 'slider_text_en', '[\"207 - 209 Tr\\u1ea7n \\u0110\\u0103ng Ninh, D\\u1ecbch V\\u1ecdng, C\\u1ea7u Gi\\u1ea5y, H\\u00e0 N\\u1ed9i\",\"trangdo@aatour.vn\",\"098985 359\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"<div class=\\\"footer-contact-container\\\">\\r\\n                                    <span class=\\\"text-bold color-green text-uppercase company\\\">c\\u00f4ng ty cp th\\u01b0\\u01a1ng m\\u1ea1i v\\u00e0 d\\u1ecbch v\\u1ee5 du l\\u1ecbch anh anh<\\/span>\\r\\n                                    <span class=\\\"text-capitalize\\\">\\u0111\\u1ecba ch\\u1ec9: 207 - 209 tr\\u1ea7n \\u0111\\u0103ng ninh, d\\u1ecbch v\\u1ecdng, c\\u1ea7u gi\\u1ea5y, h\\u00e0 n\\u1ed9i<\\/span>\\r\\n                                    <span>\\u0110i\\u1ec7n tho\\u1ea1i: <a href=\\\"tel:0243 793 9363\\\">0243 793 9363<\\/a><\\/span>\\r\\n                                    <span>Email: <a href=\\\"#\\\">trangdo@aatour.vn<\\/a><\\/span>\\r\\n                                <\\/div>\"]');
INSERT INTO `tbl_setting` VALUES (27, 'slider_text_fr', '[\"207 - 209 Tr\\u1ea7n \\u0110\\u0103ng Ninh, D\\u1ecbch V\\u1ecdng, C\\u1ea7u Gi\\u1ea5y, H\\u00e0 N\\u1ed9i\",\"trangdo@aatour.vn\",\"098985 359\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"https:\\/\\/www.facebook.com\\/aatourspr\",\"<div class=\\\"footer-contact-container\\\">\\r\\n                                    <span class=\\\"text-bold color-green text-uppercase company\\\">c\\u00f4ng ty cp th\\u01b0\\u01a1ng m\\u1ea1i v\\u00e0 d\\u1ecbch v\\u1ee5 du l\\u1ecbch anh anh<\\/span>\\r\\n                                    <span class=\\\"text-capitalize\\\">\\u0111\\u1ecba ch\\u1ec9: 207 - 209 tr\\u1ea7n \\u0111\\u0103ng ninh, d\\u1ecbch v\\u1ecdng, c\\u1ea7u gi\\u1ea5y, h\\u00e0 n\\u1ed9i<\\/span>\\r\\n                                    <span>\\u0110i\\u1ec7n tho\\u1ea1i: <a href=\\\"tel:0243 793 9363\\\">0243 793 9363<\\/a><\\/span>\\r\\n                                    <span>Email: <a href=\\\"#\\\">trangdo@aatour.vn<\\/a><\\/span>\\r\\n                                <\\/div>\"]');
INSERT INTO `tbl_setting` VALUES (28, 'featured_tour_taxonomy_fr', 'null');
INSERT INTO `tbl_setting` VALUES (29, 'featured_tour_fr', '[\"174\",\"173\",\"172\",\"171\",\"170\",\"169\",\"168\"]');
INSERT INTO `tbl_setting` VALUES (30, 'featured_news_fr', '[\"103\",\"95\",\"99\",\"98\"]');
INSERT INTO `tbl_setting` VALUES (31, 'featured_theme_fr', '[\"24\",\"22\",\"23\"]');
INSERT INTO `tbl_setting` VALUES (32, 'home_meta_title_fr', NULL);
INSERT INTO `tbl_setting` VALUES (33, 'home_meta_keyword_fr', NULL);
INSERT INTO `tbl_setting` VALUES (34, 'home_meta_description_fr', NULL);
INSERT INTO `tbl_setting` VALUES (35, 'hotel_more_vi', NULL);
INSERT INTO `tbl_setting` VALUES (36, 'hotel_more_en', NULL);
INSERT INTO `tbl_setting` VALUES (37, 'hotel_more_fr', NULL);
INSERT INTO `tbl_setting` VALUES (38, 'theme_more_vi', NULL);
INSERT INTO `tbl_setting` VALUES (39, 'theme_more_en', NULL);
INSERT INTO `tbl_setting` VALUES (40, 'theme_more_fr', NULL);
INSERT INTO `tbl_setting` VALUES (41, 'schedule_title_vi', NULL);
INSERT INTO `tbl_setting` VALUES (42, 'schedule_title_en', NULL);
INSERT INTO `tbl_setting` VALUES (43, 'schedule_title_fr', NULL);
INSERT INTO `tbl_setting` VALUES (44, 'program_title_vi', NULL);
INSERT INTO `tbl_setting` VALUES (45, 'program_title_en', NULL);
INSERT INTO `tbl_setting` VALUES (46, 'program_title_fr', NULL);
INSERT INTO `tbl_setting` VALUES (47, 'footer_about_vi', NULL);
INSERT INTO `tbl_setting` VALUES (48, 'footer_about_en', NULL);
INSERT INTO `tbl_setting` VALUES (49, 'footer_about_fr', NULL);
INSERT INTO `tbl_setting` VALUES (50, 'duration_vi', '3 ngày 2 đêm,4 ngày 3 đêm');
INSERT INTO `tbl_setting` VALUES (51, 'duration_en', NULL);
INSERT INTO `tbl_setting` VALUES (52, 'price_range_vi', 'Dưới 2tr,Từ 2tr-3tr,Từ 3tr-5tr');
INSERT INTO `tbl_setting` VALUES (53, 'price_range_en', NULL);
INSERT INTO `tbl_setting` VALUES (54, 'hobby_vi', 'Picnic,Phượt,Nghỉ dưỡng');
INSERT INTO `tbl_setting` VALUES (55, 'hobby_en', NULL);

SET FOREIGN_KEY_CHECKS = 1;
