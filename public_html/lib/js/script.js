jQuery(document).ready(function($) {

	$('#close-success-modal').click(function() {
		$('#success-modal').remove()
	})

	$('span.btn-menu').click(function() {
		$('.menu-navigation').slideToggle();
	});
	$(window).bind('scroll', function () {
		if ( $(window).width() > 992 ) {
			if ($('.related-list').length > 0) {
				var height = ( $('.related-list').offset().top ) - 600;
				var oftop = $(window).scrollTop();
				if ( oftop > 0 && oftop < height) {
					$('.sidebar-detail-list').css('top', oftop);
				} else if ($( window).scrollTop() >= height ) {
					$('.sidebar-detail-list').css('top', height);
				} else {
					$('.sidebar-detail-list').css('top', 0);
				}
			}
	    }

	});

	$('body').on('click', '#reset-filter', function() {
		location.reload();
	})

	$('body').on('click', '#home-search-submit', function () {
		var v = $('#home-select-term').val();
		if (v != '') {
			window.location.href = v;
		}
	})

	$('.collapse-content').click(function() {
		$(this).next().toggle(400);
	})
	
	$(function() {
		var headerHeight = $('.header').outerHeight()
	  $('.roll-href').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
          		scrollTop: target.offset().top - headerHeight
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	
	
});

// map maker
function myMap() {
  var myCenter = new google.maps.LatLng(51.508742,-0.120850);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 13};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);
  google.maps.event.addListener(marker,'hover',function() {
    var infowindow = new google.maps.InfoWindow({
      content:"Hello World!"
    });
  infowindow.open(map,marker);
  });
}