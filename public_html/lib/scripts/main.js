(function ($) {
  
    function mansory_recent_work() {

        var $container = $('#iw-isotope-main').isotope({
            itemSelector: '.element-item'
        });
        $container.imagesLoaded().progress(function () {
            $container.isotope('layout');
        });

        $('#filters a').click(function () {
            var selector = $(this).attr('data-filter');
            $('#filters a').removeClass('active');
            $(this).addClass('active');
            $container.isotope({ filter: selector });
            return false;
        });
    }

     var pageSection = $('.slide-bg-image');
      pageSection.each(function (indx) {
          if ($(this).attr('data-background-img')) {
              $(this).css('background-image', 'url(' + $(this).data('background-img') + ')');
          }
          if ($(this).attr('data-bg-position-x')) {
              $(this).css('background-position', $(this).data('bg-position-x'));
          }
          if ($(this).attr('data-height')) {
              $(this).css('height', $(this).data('height') + 'px');
          }
          var w_window = $(window).width();
          if (w_window <= 767) {
              if ($(this).attr('data-height')) {
                  $(this).css('height', $(this).data('height') * 0.7 + 'px');
              }
          }
      });

    var $win = $(window),
        $body_m = $('body'),
        $navbar = $('.navbar');
    // **********************************************************************//
    // ! Window ready
    // **********************************************************************//
    $(document).ready(function () {

      $('.dotdotdot').dotdotdot();

      $('.navbar-toggler').click(function(){
        $('body').addClass('open-menu');
      });

      $('.close-menu').click(function(){
        $('body').removeClass('open-menu');
      });

      $('.mm-btn_next').click(function(){
        var id_block = $(this).attr('href');    
        $('.menu-panel').addClass('hidden').removeClass('opened');
        $(id_block).removeClass('hidden').addClass('opened');        

        $('.mm-btn_prev').removeClass('hidden');

        var content = $(this).parent().find('.nav-link').text();
        console.log(content);
        $('<span class="mm-separator">/</span><span>' + content + '</span>').insertAfter('.navbar-breadcrum span:last-child');
        return false;
      });

      $('.mm-btn_prev').click(function(){
        var id_current = $('.menu-panel.opened').attr('id');
        $('.menu-panel').addClass('hidden').removeClass('opened');
        $('.menu-panel .mm-btn_next').each(function(){
          if ($(this).attr('href') == ('#'+id_current)){
            $(this).parents('.menu-panel').addClass('opened').removeClass('hidden');

            if ($(this).parents('.menu-panel').hasClass('menu-main')){
              $('.mm-btn_prev').addClass('hidden');
            }
            $('.navbar-breadcrum span:last-child').remove()
            $('.navbar-breadcrum span:last-child').remove()
          }          
        });
        
      });

      
      $('.carousel-center').slick({
        speed: 2000,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        arrows: true,  
        centerMode: true,
        centerPadding: '160px',
        responsive: [
          {
            breakpoint: 1400,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              centerMode: true,
              centerPadding: '80px'
            }
          },
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              centerMode: false,
              centerPadding: '0'
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              centerMode: false,
              centerPadding: '0'
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: false,
              centerPadding: '0'
            }
          }
        ]
      });

      $('.group3').slick({
        speed: 2000,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
      
      $('.toggle-h4').click(function(){
        $(this).parent().toggleClass('open');
      });

      $('.list-brands').slick({
        slidesToShow:7,
        slidesToScroll:1,
        arrows:false,
        dots:false,
        autoplay:true,
         responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 6
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 5
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 4
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 360,
            settings: {
              slidesToShow: 2
            }
          }

          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });

      // Bootstrap Dropdown 
        var $dropdown_menu = $('.navbar-main .dropdown');
        if ($dropdown_menu.length > 0) {
            $dropdown_menu.on('mouseover', function () {
                if ($win.width() > 991) {
                    $('.dropdown-menu', this).not('.in .dropdown-menu').stop().fadeIn('400');
                    $(this).addClass('open');
                }
            });
            $dropdown_menu.on('mouseleave', function () {
                if ($win.width() > 991) {
                    $('.dropdown-menu', this).not('.in .dropdown-menu').stop().fadeOut('400');
                    $(this).removeClass('open');
                }
            });
            $dropdown_menu.on('click', function () {
                if ($win.width() < 991) {
                    $(this).children('.dropdown-menu').fadeToggle(400);
                    $(this).toggleClass('open');
                    return false;
                }
            });
        }

        mansory_recent_work();

      $(window).scroll(function(){
        if ($(this).scrollTop() > 150){
          $('body').addClass('fixed-header');
        }
        else{
          $('body').removeClass('fixed-header');
        }
      });


      // Count Down
      var $count_token = $('.token-countdown');
      if ($count_token.length > 0) {
          $count_token.each(function () {
              var $self = $(this),
                  datetime = $self.attr('data-date');
              $self.countdown(datetime).on('update.countdown', function (event) {
                  $(this).html(event.strftime('' + '<span class="countdown-label">Còn</span>' + '<span class="countdown-time countdown-time-first">%D</span><span class="countdown-text"> ngày </span>' + '<span class="countdown-time">%H</span><span class="countdown-text">:</span>' + '<span class="countdown-time">%M</span><span class="countdown-text">:</span>' + '<span class="countdown-time countdown-time-last">%S</span>'));
              });
          });
      }

      $('.slider-banner').slick({
        fade:true,
        dots:false,
        arrows:false,
        autoplay:true,
        speed:2000
      });

      $('.custom-select').customselect({
        search:true
      });

      $('[data-toggle="datepicker"]').datepicker({
        'dateFormat':'dd/mm/yy',
        // Days' name of the week.
        days: ['Chủ Nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],

        // Shorter days' name
        daysShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

        // Shortest days' name
        daysMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

        // Months' name
        months: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],

        // Shorter months' name
        monthsShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12']
      });


      $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
      });
      $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        focusOnSelect: true,
        arrows:true,
        responsive: [
          {
            breakpoint: 566,
            settings: {
              slidesToShow: 3
            }
          }
        ]
      });

      $('.slider-related-tour').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows:true,
        speed:2000,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 566,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });

      $('.slider-tour-special').slick({
        slidesToShow:1,
        slidesToScroll:1,
        arrows:false,
        dots:true,
        speed:2000,
        responsive: [
          {
            breakpoint: 991,
            settings: {
              rows:2,
              slidesPerRow:1
            }
          },
          {
            breakpoint: 768,
            settings: {
              rows:1,
              slidesPerRow:2
            }
          },
          {
            breakpoint: 575,
            settings: {
              rows:1,
              slidesPerRow:1
            }
          }
        ]
      })
    });
  
    // **********************************************************************//
    // ! Window load
    // **********************************************************************//
    $(window).on('load', function () {
  
    });
  
    // **********************************************************************//
    // ! Window resize
    // **********************************************************************//
    $(window).on('resize', function () {
       var pageSection = $('.slide-bg-image');
      pageSection.each(function (indx) {
          if ($(this).attr('data-background-img')) {
              $(this).css('background-image', 'url(' + $(this).data('background-img') + ')');
          }
          if ($(this).attr('data-bg-position-x')) {
              $(this).css('background-position', $(this).data('bg-position-x'));
          }
          if ($(this).attr('data-height')) {
              $(this).css('height', $(this).data('height') + 'px');
          }
          var w_window = $(window).width();
          if (w_window <= 767) {
              if ($(this).attr('data-height')) {
                  $(this).css('height', $(this).data('height') * 0.7 + 'px');
              }
          }
      });
    });
  



  })(jQuery);
  
  
  
  