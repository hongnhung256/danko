jQuery(document).ready(function($) {
    $('body').on('change', '#checkAllCategory', function() {
        $(".checkCategory").prop('checked', $(this).prop("checked"));
    });

    $('body').on('click', '#deleteAllTaxonomy', function() {
        var numberOfChecked = $('input:checkbox:checked').length;
        if (numberOfChecked > 0) {
            $('#deleteAll').submit();
        }
    });

    $('input#meta_title').maxlength({
        alwaysShow: true,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger",
        separator: ' trên tổng số ',
        preText: 'Bạn đã dùng ',
        postText: ' ký tự cho phép.',
        validate: true
    });

    $('textarea#meta_description').maxlength({
        alwaysShow: true,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger",
        separator: ' trên tổng số ',
        preText: 'Bạn đã dùng ',
        postText: ' ký tự cho phép.',
        validate: true
    });

    $('body').on('click', '#price-table-add-more', function() {
        var html = '<div class="col-xs-12">' +
            '<div class="col-xs-12 m-t-15 m-b-15" style="padding: 0">' +
            '<a href="javascript:;" class="pull-right label label-danger delete-price-table-item" style="max-width: 100px">Xóa</a>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-xs-6">' +
            '<label>Tiêu đề</label>' +
            '<input name="tour_price_table_title[]" id="tour_price_table_title" class="form-control">' +
            '</div>' +
            '<div class="col-xs-6">' +
            '<label>Nội dung</label>' +
            '<input name="tour_price_table_content[]" id="tour_price_table_content" class="form-control">' +
            '</div>' +
            '</div>' +
            '</div>';
        $('#price-table-add-more').next().append(html);

        $('.delete-price-table-item').click(function() {
            $(this).parent().parent().empty();
            $.Notification.notify('error','top right','Xóa bảng giá thành công', '')
        });
    });
    
    $('.delete-price-table-item').click(function() {
        $(this).parent().parent().empty();
        $.Notification.notify('error','top right','Xóa bảng giá thành công', '')
    });

    $('#add-more-partner').click(function() {
        var html = '<div class="col-lg-4">' +
            '<div class="form-group">' +
            '<a href="javascript:;" class="pull-right label label-danger remove-partner">Xóa</a>' +
            '<label for="partner_link">Link liên kết</label>' +
            '<input type="text" name="partner_link[]" class="form-control" required/>' +
        '</div>' +
        '<div class="form-group">' +
            '<label for="partner_link">Logo đối tác</label>' +
            '<input type="file" class="filestyle" data-iconname="fa fa-cloud-upload" name="partner_logo[]" required>' +
        '</div>' +
        '</div>'
        $(this).parent().parent().append(html);
        $('.filestyle').filestyle()

        $('.remove-partner').click(function() {
            $(this).parent().parent().remove() ;
        });
    });

    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});


