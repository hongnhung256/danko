jQuery(document).ready(function($) {
    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {

        //Warning Message
        $('.delete-sweetalert-warning').click(function(){
            var href = $(this).attr('data-href');
            var textAlert = $(this).attr('data-text-alert');
            var textSuccess = $(this).attr('data-text-success');
            swal({
                title: "Bạn có chắc chắn xóa?",
                text: textAlert,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Có, hãy xóa!",
                closeOnConfirm: false
            }, function(){
                swal({
                    title: "Đã xóa!",
                    text: textSuccess,
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                }, function() {
                    window.location.href = href;
                })
            });
        });
    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    $.SweetAlert.init()
}); 
